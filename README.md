# Custom Logo

## Summary

This project is proposed as a repository for project logos developed under the Project Browser initiative.

## Background

Currently project logos must use the PNG image format. However many logos have been developed as vector images. In many cases the vector image will have a smaller file size and produce a higher quality image, scalable to any resolution. It is hoped that SVG images may be supported by Project Browser in the future.

The aim of this project is to ensure that the SVG images will be available if they are needed in the future, either to produce a rescaled bitmap, or to include the vector image in the project.
